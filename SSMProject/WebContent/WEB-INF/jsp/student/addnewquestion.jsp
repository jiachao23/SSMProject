<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>添加新问题</title>
		<link rel="stylesheet" href="../css/main.css" />
		<link rel="stylesheet" href="../css/reset.css" />
	</head>

	<body>
		<header class="headerBar">
			<a href="studentindex.jsp" class="welcome_tit fl">西工答疑 · 知识之渊</a>
			<span class="active_user">当前用户<i class="user_name">末日</i>&nbsp;<a href="mygrade.jsp">我的积分</a></span>
		</header>
		<div class="hr_35"></div>
		<div class="listTit comWidth">
			<div class="hr_25"></div>
			<div class="addAnswer">
				<h3 class="addAnswer_title">我要提问</h3>
				<form action="" method="post">
					<input type="text" name="question_title" class="addAnswer_content question_title" placeholder="*输入标题" />
					<textarea type="text" name="question_details" class="addAnswer_content" placeholder="详细描述"/></textarea>
					<span class="select_teacher">
						向老师求助：
						<input type="radio" name="askteacher" id="askteacher" value="t1" class="select_item"/><i class="teacher_name">零老师</i>
						<input type="radio" name="askteacher" id="askteacher" value="t2" class="select_item"/><i class="teacher_name">一老师</i>
						<input type="radio" name="askteacher" id="askteacher" value="t3" class="select_item"/><i class="teacher_name">二老师</i>
						<input type="radio" name="askteacher" id="askteacher" value="t4" class="select_item"/><i class="teacher_name">三老师</i>
					</span>
					
					<button type="submit" class="addAnswer_btn" value="">发布问题</button>
				</form>
			</div>
			
		</div>
	</body>

</html>