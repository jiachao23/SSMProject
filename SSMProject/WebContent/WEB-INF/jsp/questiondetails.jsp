<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>问题详情</title>
		<link rel="stylesheet" href="css/main.css" />
		<link rel="stylesheet" href="css/reset.css" />
	</head>

	<body>
		<header class="headerBar">
			<h3 class="welcome_tit fl">西工答疑 · 知识之渊</h3>
		</header>
		<div class="hr_35"></div>
		<div class="listTit comWidth">
			<div class="q_title_content">
				<h3 class="question_title">求解Java对象与类</h3>
				<div class="q_details">
					<h2 class="q_descript">详细描述</h2> 前天编写程序，突然发现import java.until.前面总是有黄色的感叹号，提示我import java.until. is never used，但是我运行程序后，控制台死活显示不出内容，但是停止键又是亮着的，Scanner sc=new Scanner(System.in)以下的语句，都无法显示，不知道怎么回事，第一次遇见，没有提示error，没有红色感叹号，有谁能帮忙解决啊，停止重启eclipse，或者重新安装都没用!以前没用出现过这种情况，第一次碰到，实在没辙了,语句应该没事，是突然出现这个问题的
					<br />
					<input type="text" value="提问者" class="q_author" /><span class="answer_user">130611106</span>
				</div>
			</div>
			<div class="hr_25"></div>
			<div class="addAnswer">
				<h3 class="addAnswer_title">添加回答</h3>
				<form action="" method="post">
					<textarea type="text" name="answercon" class="addAnswer_content" /></textarea>
					<button type="submit" class="addAnswer_btn" value="">提交答案</button>
				</form>
			</div>
			<div class="hr_25"></div>
			<ul class="answer_list">
				<li class="answer_item">
					<h3 class="answer_num"><span>3</span>回答</h3></li>
				<li class="answer_item">
					<span class="answer_title">#1<i class="answer_user">130611106</i>&nbsp;&nbsp;&nbsp;回答：</span>
					<span class="answer_content">现在，我们深入研究什么是对象。如果考虑到现实世界中，可以发现身边很多对象，汽车，狗，人等，这些对象都有一个状态和行为。

							如果我们考虑一只狗，那么它的状态是 - 名称，品种，颜色和行为 - 吠叫，摇摆，跑等

							如果软件对象与现实世界中的对象进行比较，它们具有非常相似的特征。
							
							软件对象也有状态和行为。软件对象的状态存储在字段和行为是通过方法如图所示。
							
							因此，在软件开发中，方法上的一个对象的内部状态和操作的对象对对象的通信是通过方法来完成。</span>
					<span class="answer_time">
						&nbsp;&nbsp;2016-01-01 18：16：00
						<form action="" method="post" class="fr" id="agreeForm">
							<button type="submit" value="" class="answer_btn">赞同</button>
						</form>
						<form action="" method="post" class="fr" id="reportForm">
							<button type="submit" value="" class="answer_btn">举报</button>
						</form>
					</span>
				</li>
				<li class="answer_item">
					<span class="answer_title">#2<i class="answer_user">130611106</i>&nbsp;&nbsp;&nbsp;回答：</span>
					<span class="answer_content">现在，我们深入研究什么是对象。如果考虑到现实世界中，可以发现身边很多对象，汽车，狗，人等，这些对象都有一个状态和行为。

							如果我们考虑一只狗，那么它的状态是 - 名称，品种，颜色和行为 - 吠叫，摇摆，跑等

							如果软件对象与现实世界中的对象进行比较，它们具有非常相似的特征。
							
							软件对象也有状态和行为。软件对象的状态存储在字段和行为是通过方法如图所示。
							
							因此，在软件开发中，方法上的一个对象的内部状态和操作的对象对对象的通信是通过方法来完成。</span>
					<span class="answer_time">
						&nbsp;&nbsp;2016-01-01 18：16：00
						<form action="" method="post" class="fr" id="agreeForm">
							<button type="submit" value="" class="answer_btn">赞同</button>
						</form>
						<form action="" method="post" class="fr" id="reportForm">
							<button type="submit" value="" class="answer_btn">举报</button>
						</form>
					</span>
				</li>
				<li class="answer_item">
					<span class="answer_title">#3<i class="answer_user">130611106</i>&nbsp;&nbsp;&nbsp;回答：</span>
					<span class="answer_content">现在，我们深入研究什么是对象。如果考虑到现实世界中，可以发现身边很多对象，汽车，狗，人等，这些对象都有一个状态和行为。

							如果我们考虑一只狗，那么它的状态是 - 名称，品种，颜色和行为 - 吠叫，摇摆，跑等

							如果软件对象与现实世界中的对象进行比较，它们具有非常相似的特征。
							
							软件对象也有状态和行为。软件对象的状态存储在字段和行为是通过方法如图所示。
							
							因此，在软件开发中，方法上的一个对象的内部状态和操作的对象对对象的通信是通过方法来完成。</span>
					<span class="answer_time">
						&nbsp;&nbsp;2016-01-01 18：16：00
						<form action="" method="post" class="fr" id="agreeForm">
							<button type="submit" value="" class="answer_btn">赞同</button>
						</form>
						<form action="" method="post" class="fr" id="reportForm">
							<button type="submit" value="" class="answer_btn">举报</button>
						</form>
					</span>
				</li>
			</ul>
		</div>
	</body>

</html>