package com.kkk.dao;

import org.apache.ibatis.annotations.Select;

import com.kkk.entity.Student;

public interface IStudentDao {

	@Select("SELECT * FROM student_tab WHERE studentId=#{studentId}")
	public Student findStudentByStudentId(String studentId);

	@Select("SELECT * FROM student_tab WHERE id=#{id}")
	Student selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Student record);

	int updateByPrimaryKey(Student record);

}
