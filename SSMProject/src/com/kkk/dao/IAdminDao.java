package com.kkk.dao;

import org.apache.ibatis.annotations.Select;

import com.kkk.entity.Admin;

public interface IAdminDao {
	
	int deleteByPrimaryKey(Integer id);

	int insert(Admin record);

	int insertSelective(Admin record);
	@Select("SELECT * FROM admin_tab WHERE adminId=#{adminId}")
	Admin selectByPrimaryAdminId(String adminId);
	
	Admin selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Admin record);

	int updateByPrimaryKey(Admin record);
}