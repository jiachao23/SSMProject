package com.kkk.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.kkk.dao.IStudentDao;
import com.kkk.entity.Student;
import com.kkk.service.IStudentService;

@Service("StudentService")
public class StudentServiceImpl implements IStudentService {

	@Resource
	private IStudentDao studentdao;

	@Override
	public Student getUserById(int id) {
		return this.studentdao.findStduentById(id);
	}

}
